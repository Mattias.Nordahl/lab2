package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;


public interface IFridge {

	/**
	 * Returns the number of items currently in the fridge
	 * 
	 * @return number of items in the fridge
	 */
	int nItemsInFridge();

	/**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
	int totalSize();

	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	boolean placeIn(FridgeItem item);

	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
	void takeOut(FridgeItem item);

	/**
	 * Remove all items from the fridge
	 */
	void emptyFridge();

	/**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
	List<FridgeItem> removeExpiredFood();

}


class Fridge implements IFridge {
	//int maxItems = 20;
	List<FridgeItem> groceries = new ArrayList<FridgeItem>();
	@Override
	public int nItemsInFridge() {
		// TODO Auto-generated method stub
		return groceries.size();
	}

	@Override
	public int totalSize() {
		// Total size of fridge items is set to 20 items
		return 20;
	}
	@Override
	public boolean placeIn(FridgeItem item) {
		// Places items in the fridge
		if(totalSize() > nItemsInFridge()) {
			groceries.add(item);
			return true;
		}
		return false;
	}
	@Override
	public void takeOut(FridgeItem item) {
		// Takes items out of the fridge
		if (groceries.contains(item)) {
			groceries.remove(item);
		}
		else {
			throw new NoSuchElementException();
		}
		
	}
	@Override
	public void emptyFridge() {
		// Takes all items out of fridge
		groceries.clear();
		
	}
	@Override
	public List<FridgeItem> removeExpiredFood() {
		// Removes expired items
		ArrayList<FridgeItem> GroceriesExpired = new ArrayList<>();
		for (int i = 0; i < nItemsInFridge(); i++) {
			if(groceries.get(i).hasExpired()){
				GroceriesExpired.add(groceries.get(i));
				takeOut(groceries.get(i));
				i -= 1;
			}
		}
		return GroceriesExpired;
	}
}

